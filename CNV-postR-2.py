#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
# prg to calculate and provide a list of CNV genes according to the frequence of significant
# probes per gene sequence 
 
"""
compile a file containing the probes likely included in a CNP according to several parameters

Usage:
	python CNV-postR-2.py file.CNV param1

file.CNV = file containing all infos coming from the R analyses
param1 = a float fixing the frq of probes giving the same signal for genes: 0.5<param1<1.0
"""

import sys, fileinput
import string
import math
import os
import time
import random
import sys
import operator
import copy
from collections import defaultdict

try:
  g = sys.argv[1]
  p1 = sys.argv[2]
  
except:
  print __doc__
  sys.exit(1)


#naming
h = g[:-4]
output1 = h + "-probelist-CNVgene"
output2 = h + "-CNVgenelist"

f = open(sys.argv[1],'r')

#print g[:-4]

o1 = open(output1, 'w')
o2 = open(output2, 'w')

l = f.readline()

l = l.strip().split(',')
print>>o1, ','.join(l),',"slide"'


DA = {}
dag = {}
dG = {}
lG = []

for line in f:
  line = line.replace('"','')
  l = line.strip().split(',')
  l.append(h+'\n')
#dict of line for each probe
  DA[l[8]] = ','.join(l)

#dicts of probes for each gene
  if l[10] in dag.keys():
    dag[l[10]].append(l[8])
  else:
    dag[l[10]] = [l[8]]

#dict of significative probes values per gene
  if int(l[19]) != 0:
    if l[10] in dG.keys():
      dG[l[10]].append(l[8])
    else:
      dG[l[10]] = [l[8]]
  else:
    pass


#create list of CNV genes
for w in dG:
  r = float(len(dG[w]))/float(len(dag[w]))
#  print r
  if r >= float(p1):
    lG.append(w)
  else:
    pass

print>>o2, len(lG)
print>>o2,'\n'.join(lG)


#create file with all significant probes to further analyses
a = sorted(DA.keys())
for y in a:
  l = DA[y]
  l = l.replace("'","")
#  print l
  m = l.strip().split(',')
#  print m
#  print m[9]
  if m[10] in lG:
    o1.write(l)
  else:
    pass


f.close()
o1.close()
o2.close()
