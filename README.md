# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a series of programs allowing to detect genes in copy number variation from aCGH between a reference genome and a test genome in the case of species without high quality reference genomes.

To get details regarding the entire approach, read Prunier et al. 2017 in BMC Genomics : CNVs into the wild: Screening the genomes of conifer trees (picea spp.) reveals fewer gene copy number variations in hybrids and links to adaptation.

For a fast start you can focus on the 'data treatment' section in the "material&Methods".

### How do I get set up? ###

After hybridization with two different dyes, the array is scanned which produces an image that is afterwards analysed with a specific software (usually linked to the scanner) to assessed the relative fluorescence intensity for each dye at each spot (=oligonucl.). At the end of the image analysis, one should obtain a file organized in columns of values for both dyes including Raw Intensities, Background intensities, and Net Intensities.

list of required files:
           - file with the raw, background and net intensities  
           - file with reference gene name for each probe name  
           - file including the ATCG probe composition  
           - file including the gene GC content for each gene  

Each array is analysed separately, although many arrays can be analysed simultaneously on different CPUs.

The processing with python and R scripts is divided in 4 steps:

           step 1: run the CNV-prepreR.py  --> output: 'arrayname_out'  
           step 2: run the CNV-preR.py with arrayname_out as inputfile --> output: 'arrayname_out2'  
           step 3: run the CNV-R program in R with arrayname_out2 as input file --> output 'arrayname_CNV'  
           step 4: run the CNV-postR.py with arrayname which provides 2 outputfiles:  
                      - list of probes within a CNV gene with all information (intensities, GC content... etc...)  
                      - list of gene names of the CNV genes  


Notes:

- each python and R file (*.py and CNV-R) includes a purpose and usage descriptions at the beginning.
- output files (*_out, *_out2, *.CNV...) are provided to allow users making sure they got the expected results and file format.


### Who do I talk to? ###

* Repo owner
* No other community or team contact to date