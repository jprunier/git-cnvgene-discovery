#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
# prg to add an reference gene name at the end of each line according
# to the probe ID
# requires an intensity file and a dictionnary with probe IDs and corresponding gene names

"""
Description:

add 1 column: reference gene name for each probe of an intensity file

Usage:
	python CNV_prepreR.py Intensity_file probe/gene_dictionnary

Intensity_file: the output of the CGH array scan analysis
probe/gene_dictionnary : file with the probe IDs and corresponding gene names

"""


import sys, fileinput
import string
import math
import os
import time
import random
import sys
import operator
import copy
from collections import defaultdict


try:
	input = sys.argv[1]
	table = sys.argv[2]

except:
	print __doc__
	sys.exit(1)



g = open(table,'r')

#dictionnary with probe names and gene names

dict= {}

l= g.readline()

for line in g:
	l = line.split('\t')
	dict[l[0]] = l[1]

g.close()

f = open(input, 'r')

out = input[:-4]

o = open(out+"_out",'w')

#prepare the first line
line = f.readline()
line = line.replace('"','')
line = line.replace(' ','')
line = line.strip().split('\t')
line = line[:-1]
print>>o, ','.join(line), ",GeneName"

#subsequent lines
for line in f:
	line = line.replace('"','')
	line = line.replace(',,',',')
	line = line.replace('\r','')
	l = line.strip().split('\t')
#	print l
#	print l[9]
	if l[9] in dict.keys():
		l.append(dict[l[9]])
		o.write(','.join(l))
		o.write('\n')
	else:
		pass
	
f.close()
o.close()	