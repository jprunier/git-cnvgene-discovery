#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
# prg to add in an intensity file the probe ATCG composition and the gene GC
# content for each CGH probe 
# 
"""

add 5 columns: nA, nT, nC, nG, GC_content

Usage:
	python CNV-preR.py table_probe_composotion table_gene_GCcontent Intensity_file

table_probe_composition : table containing 5 columns: probe_name and nA, nT, nC, nG

table_gene_GCcontent: table containing 2 columns: gene_name and GC_content

Intensity_file : file containing the output of CGH array scan
	

"""


import sys, fileinput
import string
import math
import os
import time
import random
import sys
import operator
import copy
from collections import defaultdict


try:
	f = sys.argv[1] #table of probe composition
	g = sys.argv[2] #table of gene GC content
	h = sys.argv[3] #intensity file

except:
	print __doc__
	sys.exit(1)


# dict for probe composition
f = open(sys.argv[1],'r')

dict = defaultdict(list)

line = f.readline()

for line in f:
	l = line.strip().split('\t')
	m = [l[1], l[2], l[3], l[4]]
#	print l
	k, v = l[0], m
	dict[k] = v

f.close()

#print dict

# dict for GC content
dict_seq = defaultdict(float)

g = open(sys.argv[2], 'r')

line = g.readline()

for line in g:
	k, v = line.split()
	dict_seq[k] = v
	
#print dict_seq.keys()
g.close()


# parsing intensity datafile and add GC and ATCG in outputfile
slide = sys.argv[3]
out = slide[:-4]
#print slide

h = open(slide,'r')
o = open(out+"_out2",'w')

line = h.readline()
line = line.strip().split(",")
#print ",".join(line)
print>>o, ",".join(line), ",nA,", "nT,", "nC,", "nG,", "GCcontent"

r = {}


for line in h:
	line = line.replace('"','')
	l = line.strip().split(",")
	if l[9] in dict.keys():
	        l.extend(dict[l[9]])
		m = line.replace('_',',',5)
		m = m.strip().split(',')
		name = m[16] + '-' + m[13]
		if name in dict_seq.keys():
                        l.append(dict_seq[name])
                else:
                        l.append("false")
                o.write(",".join(l))
                o.write('\n')
        else:
		pass

h.close()
o.close()

